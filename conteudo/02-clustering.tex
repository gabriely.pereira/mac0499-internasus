%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

%% ------------------------------------------------------------------------- %%
\chapter{Clustering}
\label{cap:clustering}

We are in an era full of data. Every passing day people, companies, and government entities are collecting amounts of data to analyze and retrieve some meaningful information from them. Big Data Analytics in healthcare, for example, is evolving into a promising field for providing insight from very large datasets \citep{Raghupathi:2014}. However, the large number of data can also become an obstacle to do data analysis because of its size \citep{Tsai:2015}.

Classifying or grouping them into a set of categories is a way to solve this problem. Cluster analysis is the organization of a collection of patterns into clusters based on similarity \citep{Jain:1999}.

In terms of Big Data, it is very confusing or impossible to get something useful from the entire data. Basically, instead of dealing with this, the method simplifies them into clusters according to the similarity among these data. That is one of the most primitive activities of a human being \citep{Anderberg:1973}. To understand a new object or event, we describe its features and find similarities among other known objects or events. So, we classify them based on the similarity or dissimilarity according to some standards.

Besides the use of cluster analysis to reduce an extensive body of data to a short description, there is a wide variety of other applications. Many other fields of study are using this analysis, such as Life sciences; Medicine; Social sciences; Earth sciences; Engineering sciences; and Policy sciences \citep{Anderberg:1973}. Among all applications developed by these fields, we can mention the following cases, respectively: to construct taxonomies; for making diagnoses in the treatment of patients; to find behavior patterns; to analyze land and rock; to identify handwritten characters, and; to identify credit risk.

The variety of algorithms for grouping data into clusters is enormous. Each field of study developed its own without notice of other fields developing similar methods. We need to use some algorithm to get the clusters because the quantity of possible classifications of a dataset into groups is too large. According to \citet{Anderberg:1973}, the number of possible results when classifying a dataset containing \textit{n} items into \textit{m} clusters is a sum of \textit{n} Stirling partition numbers, where each Stirling number represents the possibilities to group \textit{n} items into a specific number of clusters, where \textit{$1 \leq m \leq n$}. All the options we would investigate is the sum below:

\centerline{$\displaystyle\sum_{m=1}^{n} S(n, m) = \sum_{m=1}^{n} \frac{1}{m!} \sum_{k=0}^{m} (-1)^{m-k} (m k)k^n$.}

For instance, the possibilities to sort 20 elements into \textit{eight} distinct clusters is the following:

\centerline{S(20, 8) = 15.170.932.662.679.}

It would take an extraordinary amount of time to analyze so many partitions, and lots of them would be uninteresting because we are considering all the possibilities of arrangements.

%% ------------------------------------------------------------------------- %%
\section{Cluster Analysis Algorithms}
\label{sec:cluster_analysis_algorithms}

Clustering is the classification of patterns into groups. It is considered an intrinsic or unsupervised classification \citep{Jain:1988}. Figure ~\ref{fig:tree_of_methods} shows a tree of classification methods, suggested by \citet{Lance:1967}. We describe each level of the tree below.

\textbf{Exclusive and non-exclusive:} The exclusive classification is a partition of the dataset into the groups, i.e., every data item belong to only one group. On the other hand, the non-exclusive or overlapping classification permits the item to belongs to more than one group. A fuzzy clustering method is one kind of non-exclusive classification. It assigns degrees of membership in several clusters to each data item.

\textbf{Intrinsic and extrinsic:} Intrinsic, or unsupervised, when only the proximity measure between items is used to obtain the classification. Outward or supervised, when classification is done using the proximity measure and the category label too. The intrinsic classification choses the groups utilizing the similarity (or dissimilarity) among the data items. The extrinsic classification choses the groups discriminating the items according to the given labels.

\textbf{Hierarchical and partitional:} A hierarchical classification is a nested sequence of partitions which can be represented in a \textit{dendrogram}. On the other hand, a partitional classification produces a single separation of the data instead of a clustering structure, such as the \textit{dendrogram} provided by the hierarchical classification.

\begin{figure}
  \centering
    \includegraphics[width=0.5\textwidth]{tree_of_methods}
  \caption{Classification methods (extracted from \citep{Jain:1988}).}
  \label{fig:tree_of_methods}
\end{figure}

\subsection{The main algorithms}
\label{subsec:the_main_algorithms}

There are two main categories of clustering algorithms, the hierarchical and the partitional methods. As discussed previously, hierarchical methods produce a nested series of partitions, whereas partitional methods produce only one.

\subsubsection{Hierarchical}
\label{subsec:hierarchical}

Most of the hierarchical algorithms are similar to the single-link \citep{Sneath:1973} and the complete-link \citep{King:1967} algorithms. Both of them follow the agglomerative way of grouping items. In other words, considering a dataset containing \textit{n} items, it starts with \textit{n} distinct groups, one for each item, and these groups are merged until a stopping condition is met.

The stopping condition is based on the similarity between clusters. The single-link distance considers the \textit{minimum} of the distances between all pairs of patterns from the two clusters. The complete-links consider the \textit{maximum} of these distances, though.

Besides this difference, both algorithms work following these steps \citep{Jain:1999}:

\begin{enumerate}
  \item Set a group for each item, build a list containing the distances between all the possible pairs of items, and sort this in ascending order.
  \item Step through the sorted list. For each distinct similarity value, form a graph connecting the pair of items closer than this value. Stop if all of the items are connected to the graph.
  \item The result of this algorithm can be represented in a \textit{dendrogram} like the one shown in Figure ~\ref{fig:dendrogram}. Every similarity level can provide a different grouping. The dashed line at Figure ~\ref{fig:dendrogram} produces the \textit{three} clusters \textit{A+B+C}, \textit{D+E}, and \textit{F+G}, for example.
\end{enumerate}

\begin{figure}
  \centering
    \includegraphics[width=0.5\textwidth]{dendrogram}
  \caption{Dendrogram obtained using a hierarchical algorithm (extracted from \citep{Jain:1999}).}
  \label{fig:dendrogram}
\end{figure}

\subsubsection{Partitional}
\label{subsec:partitional}

Partitional algorithms obtain a single partition of the data considering a specific number \textit{k} of clusters. This technique produces the clusters using a criterion function; usually, a \textit{square error method} or a \textit{graph-theoretic divisive clustering algorithm}.

The \textit{k-means} is the most common algorithm using a squared error criterion \citep{Macqueen:1967}. It has linear-complexity $O(n)$, where \textit{n} is the number of items of the dataset. This algorithm is described as the following steps:

\begin{enumerate}
  \item Choose randomly \textit{k} clusters centroids as seeds. They can be \textit{k} different items from the body of data or random points inside the feature space of the dataset.
  \item Attribute each item to the closest cluster.
  \item Recalculate the centroid of the cluster considering all items inside the cluster and calculating the midpoint among the items.
  \item If it converges, stop. We can consider it converges when we produce the same clusters as the previous result. If it does not converge, return to step 2.
\end{enumerate}

The main graph-theoretic divisive clustering algorithm is based on the \textit{minimal spanning tree} (MST) of the data \citep{Zahn:1971}.

\begin{enumerate}
  \item Construct the \textit{minimal spanning tree} of the data.
  \item Delete the largest edges of the created MST, where each edge represents the distance measure (e.g., the Euclidean distance).
  \item Stop when a specific criterion is met.
\end{enumerate}

%% ------------------------------------------------------------------------- %%
\section{Clusterization at the platform}
\label{sec:clusterization_at_the_platform}

The developed platform provides a spatial visualization through clusters (see Figure \ref{fig:advanced_search} and Section \ref{sec:features}) using georeferenced data. Once the data are displayed in a geographic map, so the similarity measure is the geographic distance where the coordinates is defined in terms of latitude and longitude.

The algorithm implemented at the platform uses the \textit{maximum} distance between the clusters. It is a kind of a complete-link algorithm (described in the Subsection \ref{subsec:the_main_algorithms}) called hierarchical greedy clustering. In the platform implementation, we are using the \textit{Leaflet} library (Leaflet.markercluster\footnote{https://github.com/Leaflet/Leaflet.markercluster}), a free software project that provides this algorithm.

The \textit{hierarchical greedy clustering} algorithm, described by \citep{Agafonkin:2016}, intends to resolve the grouping of the data for each zoom level. These steps show how it works:

\begin{enumerate}
  \item Choose an item, represented by geographical coordinates.
  \item Find the items around the chosen item within a certain radius.
  \item Make a cluster with those items. The cluster is represented by its centroid (the coordinates of the first chosen item).
  \item Choose an item that is not part of any cluster.
  \item Repeat until every item is visited.
\end{enumerate}

This approach becomes too expensive when we need to process the entire dataset for each zoom level, following those steps. The algorithm avoids this problem by reusing calculations. For instance, consider we need to follow those steps for 18 different zoom levels. Instead of recalculating every cluster, we can reuse the calculation of the last zoom level to calculate the next one. 

The Figure ~\ref{fig:greedy_clustering} show how it works. So, we start from the deeper zoom level (\textit{z18} in the figure case), where we extract more clusters. The next level (\textit{z17}) reuses the clusters from the last zoom level (\textit{z18}), considering them as the original items from the dataset. This process is repeated for each zoom level, and it is much faster than the simple solution and can be fast enough to handle millions of points on the map.

\begin{figure}
  \centering
    \includegraphics[width=0.5\textwidth]{greedy_clustering}
  \caption{Hierarchical greedy clustering algorithm at different zoom levels (extracted from \citep{Agafonkin:2016}).}
  \label{fig:greedy_clustering}
\end{figure}

Besides, there are two expensive operations in this algorithm: Find an item that is not part of any cluster and find every cluster on the current screen. But it can be improved using a method proposed by \citet{Agafonkin:2016} called spatial index. So, instead of a loop into the dataset seeking a point every time, we can preprocess the database into a particular data structure and then use it to find any item. In this way, we can index any item from the dataset at any zoom level.

With the clustering computational approach, the platform provides for the public health professionals the ability for performing analyzes and understanding patterns.
It can help the task of finding hospitalization concentrations on the map for a specific diagnosis, for example.
This kind of analyzes is not possible without the aid of software that uses data visualization and clustering techniques.
The last part of Chapter \ref{cap:platform} (specifically in Section \ref{sec:features}) presents the usage of clusters to show spatial data in practice.
