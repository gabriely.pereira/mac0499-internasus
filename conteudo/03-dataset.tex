%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

%% ------------------------------------------------------------------------- %%
\chapter{Dataset}
\label{cap:dataset}

The Brazilian Unified Health System (\textit{Sistema Único de Saúde} -- SUS) enables the process of gathering large datasets into different health information systems, such as: National Notification System (SINAN); Mortality Information System (SIM); Information System on Live Births (SINASC); Hospital Information System (SIH-SUS); Outpatient Information System (SIA-SUS); and so on \citep{Brasil:2005}. The state finances the SUS services at the federal, state, and municipal levels. It provides essential, universal, and free access services to the entire population of the country \citep{Paim:2011}.

According to the 2013 National Health Survey (\textit{Pesquisa Nacional de Saúde} -- PNS), performed by the Brazilian Institute of Geography and Statistics (\textit{Instituto Brasileiro de Geografia e Estatística} -- IBGE), the majority of the population uses SUS services. This survey indicated that more than 80\% of the Brazilian population is dependent on SUS \citep{Stopa:2017}. The SUS datasets become vital to the awareness of the health situation in Brazil. Moreover, they can support the management of public health services.

The SUS Information Technology Department (\textit{Departamento de Informática do SUS} -- DATASUS) organizes the data and provides free access to them. DATASUS aims to integrate Health Information to the Brazilian Ministry of Health, according to a Federal Government decree on April 16th, 1991:

\begin{quote}
  \textit{\textbf{Art. 12.} It is the Information Technology Department of SUS that specifies, develops, implements and operates information systems for the core activities of SUS, following the sectoral organ guidelines.} \citep{decree100:1991} 
\end{quote}

From then on, DATASUS developed about 200 different systems (such as SINAN, SIM, SINASC, among others cited previously) to provide software solutions to improve the computerization of SUS data \citep{Brasil:2002}. The generated data are widely available through the DATASUS site\footnote{http://datasus.saude.gov.br/}.

%% ------------------------------------------------------------------------- %%
\section{Hospital Information System Dataset (SIH-SUS)}
\label{sec:hospital_information_system}

The Hospital Information System (SIH-SUS) is one of the systems developed by DATASUS. This system is composed of many filled forms from the country health facilities and it becomes available on the dataset within a month after the form filling. These forms follows the Hospital Admission Authorization (AIH) form model and they are composed of, among other things, diagnosis code for hospital admission (International Classification of Diseases - ICD-10), age group, gender, amount paid, duration time, patient location, and health facility location.

Through the SIH-SUS dataset, we have access to hospitalization data that represents about 70\% of all hospitalizations of the country \citep{Brasil:2005}. So, these data can be a good representation of the country real situation.

However, there are constraints on the accuracy and reliability of the data. Fields can suffer variations producing biased estimates. There are cases such as unreliable patient location, false diagnosis, and duplicated hospitalization data.

In spite of its limitations, this dataset is essential for the awareness of the SUS healthcare assistance and for the prioritization of precautionary measures. Its data provides meaningful information to public health managers. For example, through these data it is possible to find which region lacks hospitals, or even which hospital lacks a specific specialty service (like pediatrics).

The SIH-SUS dataset is composed of data from all over Brazil, however it may differ subtly from region to region. The form composition can be different containing distinct field names or more fields than the default AIH form. For example, the São Paulo SIH-SUS dataset contains the patient latitude and longitude coordination (inserted by São Paulo Municipal Health Secretariat), but it is not required by the AIH form. Next section explain how it works at São Paulo.

%% ------------------------------------------------------------------------- %%
\section{The São Paulo SIH-SUS dataset}
\label{sec:sih_dataset_from_sp}

The great São Paulo population estimate is about 20.935.000 people \citep{Demographia:2019}. So, nearly 10 percent of the Brazilian population (210.536.000, based on IBGE's population projection\footnote{https://www.ibge.gov.br/apps/populacao/projecao/index.html, retrieved on October 3rd 2019.}) lives in great São Paulo. As well as other megacities, great São Paulo is facing challenges such as crime, traffic congestion, urban sprawl, and also public health problems.

São Paulo city has one of the best public health care system in Brazil. Its health information management is advanced compared to most other Brazilian cities. In this context, for example, São Paulo city has specific departments in its Municipal Health Secretariat (SMS-SP), such as the Epidemiology and Information Coordination (\textit{Coordenação de Epidemiologia e Informação} -- CEInfo) created in 1989 (before DATASUS) \citep{PMSP:2016}.

The CEInfo had an essential role in the platform development. They preprocessed the SIH-SUS dataset removing the corrupted and blank data, anonymizing the entire dataset, and inserting the geolocation (in terms of latitude and longitude) of the census sector centroid of the patient location (i.e. geo-anonymizing the patient location, described at the end of the Section \ref{sec:final_architecture}).

\subsection{Dataset structure}
\label{subsec:dataset_structure}

Each hospitalization data at the SIH-SUS dataset contains information about the patient, the health facility where the patient was admitted, its localization, the patient diagnosis, and so on. This data is inserted into the dataset through an AIH form (described above in Section \ref{sec:hospital_information_system}). Figure \ref{fig:aih_form} represents part of this form.

\begin{figure}
  \centering
    \includegraphics[width=\textwidth]{aih_form}
  \caption{First part of the form used to feed the SIH-SUS dataset.}
  \label{fig:aih_form}
\end{figure}

Since 1995, the SIH-SUS datasets retrieved 285.745.862 hospitalization data according to the DATASUS TabNet system \footnote{http://www2.datasus.gov.br/DATASUS/index.php?area=0202\&id=11633}. The preprocessed SIH-SUS dataset, made available to us by CEInfo, covers 554.202 hospitalization data of São Paulo city patients from December 2014 to December 2015. This dataset is large and complex enough to be a good example for the platform usage.

\subsection{Dataset representation at the platform}
\label{subsec:dataset_representation}

The developed platform represents the SIH-SUS dataset in the way shown in Figure ~\ref{fig:model}.

\begin{figure}
  \centering
    \includegraphics[width=\textwidth]{model}
  \caption{The platform database model to receive the SIH-SUS dataset.}
  \label{fig:model}
\end{figure}

We divided this model into three main sections: Diagnosis, Location, and Procedure. The first one (Diagnosis) is responsible for cataloging the ICD-10 Categories, Subcategories, Groups, and Chapters. The Location section saves the name and the geographic data structure of the municipality subdivisions, such as Subprefectures, Administrative Sectors, Census Sectors, etc. The Procedure is the core section of the model. It contains the patient anonymized data (age, gender, educational level, etc.), health facility data (number of beds, phone number, etc.), and hospitalization data (admission date, leave date, hospitalization complexity, etc.).

That is how the platform database is structured to receive the SIH-SUS dataset. This representation makes the \textit{Procedure} model (the bottom model in Figure \ref{fig:model}) the center model, and it is the purpose because of its importance. From the procedure, we can found any information about the patient hospitalization. We make the database queries from the procedures at \textit{Advanced Search} page (more details in Section \ref{sec:features}), for example.
