%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

%% ------------------------------------------------------------------------- %%
\chapter{The Platform}
\label{cap:platform}

The current chapter presents the developed platform, its architecture, the design principles followed during the development, the main features, and some usage examples.
The platform focuses on promoting better analysis for georeferenced hospitalization data through a data visualization dashboard.
The implemented architecture aims to deal with any SIH-SUS dataset, making possible the analysis of public hospitalizations from any region of Brazil.

A good data visualization dashboard should be clear presenting the key information, should be concise summarizing all the data that matters, and must be interactive allowing an easy investigation.
The dashboard should also allow the data importation and exportation in different forms, such as PDF and CSV formats.
We are dealing with a georeferenced dataset so it is important to the platform to have he ability to visualize the data spatially.
The developed platform meet these requirements to deal with the SIH-SUS large dataset.

Below, we present the previous similar work, its results and its problems.
After this, we will discuss the possibility of using a \textit{Microservices} architecture and its pros and cons.
In the end, we will show the final platform, its features and usage examples.
% Concerning the development of this project, the architecture choice is an important decision about the platform structure.

\section{The previous solution}
\label{sec:architecture_evolution}
There was a previous version of this platform to solve the same situation (enable the visualization of a large georeferenced heath dataset).
It followed the monolithic architecture, i.e., it is a software application containing interconnected components designed without modularity.
This version started to be developed in early 2017 as a health dashboard application for São Paulo hospitalization georeferenced data.
It enables the user to filter the data, search for specifics hospitalizations and visualize this data on the São Paulo city map, just like shows Figure \ref{fig:previous_platform}.

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.9\textwidth]{previous_platform}
  \caption{The previous platform developed for health data visualization.}
  \label{fig:previous_platform}
\end{figure}

During the development, the number of added features was increasing, and so the size of the codebase. But it brought to the platform some problems like:
\begin{itemize}
  \item A significant complexity caused directly by the large codebase;
  \item Great difficulty and time consuming for a developer understand the code;
  \item Big challenge to adopt new technologies and programming languages;
  \item Difficulty to fully test the entire system, due to its complexity, and this lack of testability can release bugs to production;
  \item Disruption of the whole system if there is one failure.
\end{itemize}


\section{The microservices solution}
\label{sec:microservices_solution}
Migrating to microservices can be a good solution to solve the specific problems presented in the last section.
Microservices architecture is a service‑oriented architecture composed of loosely coupled elements that have bounded contexts \citep{Cockcroft:2014}.
It splits the code base into relatively small services for different purposes.
Among other benefits, described later in Subsection \ref{subsec:advantages_microservices}, this solution would add modularity to the platform, an essential feature when developing large applications.

Furthermore, a service‑oriented architecture can also facilitate the creation of different applications for visualization of any spatial data (see Figure \ref{fig:geomonitor_da_saude}).
It allows the use of different geo-referenced data, such as SIH, SINASC, SIM, and other datasets besides the ones cited at Chapter \ref{cap:dataset}, like safety and mobility data \citep{Pinheiro:2018}.

\begin{figure}[ht]
  \centering
    \includegraphics[width=0.6\textwidth]{geomonitor_da_saude}
  \caption{Architecture of \textit{GeoMonitor da Saúde} platform (extracted from \citep{Pinheiro:2018}).}
  \label{fig:geomonitor_da_saude}
\end{figure}

The structure of \textit{Geomonitor da Saúde}, proposed by \citet{Pinheiro:2018}, contains three main components: the applications, the service, and the datasets.
The applications offer different ways to display data from the datasets.
In this case, the platform would be just one of these applications.
The service is composed of four modules: \textit{Registro}, to receive the geo-located data from the datasets; \textit{Geoanonimização}, to anonymize the patient location; \textit{Agrupamento}, to process data into clusters; and \textit{Consulta}, to deliver this data to the applications.
The datasets presented at the bottom of Figure \ref{fig:geomonitor_da_saude} could be any dataset containing georeferenced information.

There are many factors when evaluating if this architecture is suitable for our platform~\citep{Taibi:2017}.
The two next sections discuss the advantages and disadvantages concerning the use of microservices in our case.

\subsection{Advantages to migrating to microservices}
\label{subsec:advantages_microservices}
Moving to a microservices-based approach makes application development faster and easier to manage \citep{Richardson:2016}.
It happens because of its modularity. According to \citet{Richardson:2019}, the independence of its services promotes many features to a platform.

The architecture modularity decomposes the application into a set of services. It contributes to the understanding of the code and also combats the complexity problem.
This migration also promotes deployment and scalability independence. Each service can be deployed independently on the hardware that covers its requirements. This approach would make continuous deployment possible.

The independence between the components of the system promotes the autonomy of teams, delegates placed responsibilities, removes the need for synchronization between them, and fosters the development of parallelization.
The developers are free to choose the technology to be employed for a new service. It also facilitates the refactoring of an existing service to new technology or language.
Moreover, it enables the use of different programming languages and technologies to various components.

Finally, its relative small services improve the test coverage, because of the division of the code base into modules easier to cover.
And also, the failure of one component does not disrupt the whole platform.

However, microservices are not silver bullets, like every other technology \citep{Brooks:1975}. So there are also drawbacks and issues, and we describe them below.

\subsection{Disadvantages to migrating to microservices}
\label{subsec:disadvantage_microservices}
The goal of microservices is to sufficiently decompose the application to facilitate agile application development and deployment \citep{Richardson:2016}.
This goal can benefit many different systems, but the decision of re-architecting a platform must be based on real facts and actual issues from the current application.

The migration to microservices results in some aspects to the future system, but not always our needs or available resources meet these points.
First, the decomposition of the application into different services aims to facilitate the understanding of one service for a team, because each team usually handles with few services or only one.
If there is just one team for the maintenance of the entire application, this division could increase the difficulty to understand and contribute to the system evolution.
Moreover, each component could use different technologies, and it becomes arduous to only one team deal with this kind of complexity.
In our case, we are developing this platform to be used and maintained by public entities. They could not sustain multiple teams to manage the platform like companies usually do.

Second, microservices are focused on solving industry problems, and their solutions may not be significant or even possible to us.
Each service of the architecture carries its specific deployment, resource, scaling, and monitoring requirements \citep{Richardson:2016}.
In this context, the limited available resources at public entities might be an infrastructure bottleneck and must impact important topics like deployment and scalability.

Section \ref{sec:final_architecture} shows how we get around these points with the final architecture.
But first, we expose below the design principles followed during the development.

\section{Design Principles}
\label{sec:design_principles}
These design principles are used to build a feasible, robust, and concrete application architecture. They contribute to the software extensibility and maintainability. We considered the benefits of using the design principles below as the main practices to create resilient software.

The platform needs to contain \textbf{modularity}, at some level.
It facilitates the comprehension of the code and increases its cohesion due to its division in logical components.
It also improves communication between parts of the code.
Moreover, it enables the full test coverage because it is easier to test small modules than the entire code base.

The \textbf{reuse of open source software projects} can improve faster code development, save costs, and enhance code reliability.
However, it is important to be aware of the package quality and to the use of well known open source projects with an active developer community.

We are also following the \textbf{\textit{Don't Repeat Yourself} principle} (DRY). Dave Thomas, the author of \textit{The Pragmatic Programmer}, said that the idea behind DRY is the need for every piece of knowledge in the development of something to have a single representation.
Furthermore, a piece of knowledge can be either the build system, the database schema, the tests, or even the documentation.

Finally, we believe the \textbf{internationalization} of the application is a relevant point.
It is used to provide multi-language support.
With that, we can make it easier to customize and extend the platform for other languages.
We consider other countries could want to know this platform and base on this one to develop a similar solution for them.

According to these design principles we developed the platform, described in the section below.


\section{The final solution}
\label{sec:final_architecture}
This work developed an architecture for a platform that enables the visualization of any SIH-SUS dataset.
The final architecture intends to bring benefits over the monolithic version, and overcome the microservices disadvantages, described in Subsection \ref{subsec:disadvantage_microservices}.

To suit these conditions, the software design pattern \textbf{Model–View–Controller} (MVC) were selected as the core of the platform.
This architectural pattern, first described by \citet{Krasner:1988}, is composed of three objects: \textit{Model, View}, and \textit{Controler}.
The communication between them complies with the diagram shown in Figure \ref{fig:mvc}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.45\textwidth]{MVC-Process}
  \caption{Diagram of interactions within the MVC pattern.}
  \label{fig:mvc}
\end{figure}

\textit{Model} components are those parts of the system application that simulate the application domain.
In our case, the models represent the SIH-SUS dataset inside the platform. It is described in Subsection \ref{subsec:dataset_representation}.
\textit{Views} deal with everything graphical. They display aspects of their models.
As detailed in the end of the Chapter \ref{cap:dataset}, the hospitalization \textit{Procedure} model is the main model in the platform, and it updates its view called \textit{Advanced Search} page (Section \ref{sec:features} describe this and other views).
\textit{Controllers} contain the interface between their associated models and views and the input devices. They send messages to the model and provide the interface between the model with its related views 
\citep{Krasner:1988}.

According to \cite{GOF:1995}, usually known as the ``Gang of Four'' book:
\begin{quote}
  \textit{Before MVC, user interface designs tended to lump these objects} (models, views, and controllers) \textit{together. MVC decouples them to increase flexibility and reuse.}
\end{quote}

This flexibility decreases the code complexity and brings modularity to the system.
So, the platform obtains, on some level, benefits such as code reuse, high cohesion (due to MVC logical grouping), joint development (because of the code modularity and independence), and others.
It will be better discussed in next chapter (Chapter \ref{cap:discussion}).

The end of the Section \ref{sec:hospital_information_system} describes that the SIH-SUS datasets may differ subtly from region to region, so we also need a service to receive the different SIH datasets and adapt the application according to them.
Figure \ref{fig:database_generalizer} shows the effect of the simple \textbf{database generalizer}.
It imports the SIH-SUS dataset through a CVS table containing some predefined columns.
By doing this, we can generalize any SIH-SUS dataset to make possible its visualization through the platform.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.5\textwidth]{database_generalizer}
  \caption{Diagram of the database generalizer.}
  \label{fig:database_generalizer}
\end{figure}

Moreover, the platform provides a module for \textbf{location anonymization}.
Anonymization is an essential procedure to protect individual privacy, including health data.
The location anonymization aims to generalize the patient location and keep the data reliability.
The solution used by SMS-SP is the transition of the patient location to its census sector (or census tract) centroid.
This handwork solution is too slow and expensive for SMS-SP, so we developed a module to automatically convert the patient location to the census sector centroid, in terms of latitude and longitude.

Through this architecture, we developed some essential features to the platform.
They are described in the section below.

\section{Features}
\label{sec:features}

From the imported SIH-SUS dataset into the platform, it is possible to view the hospitalizations and health facilities locations on the map.
In this project, we are using data from São Paulo city, specifically.
But it is possible to import SIH-SUS datasets from other regions of the country.

The platform is a web application, and it is divided into different pages for different purposes.
The main page is called \textbf{Advanced Search} (see Figure \ref{fig:advanced_search}), and it contains the view of the \textit{Procedure} model (as described in the end of Subsection \ref{subsec:dataset_representation}). It contains the patients' geo-anonymized locations, the locations of the health facilities they were admitted, and the hospitalization concentration by region.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.9\textwidth]{advanced_search}
  \caption{Advanced Search page displaying all São Paulo hospitalizations in 2015.}
  \label{fig:advanced_search}
\end{figure}

Inside this page, we can find the right side menu with three sections containing fields that work just like filters.
These three sections are: Health facility (\textit{Estabelecimento}), Procedure (\textit{Procedimento}), and Patient data (\textit{Informações do paciente}).
These fields enable the search of hospitalization information by the health facilities, such as its administration or its name, by the hospitalization data, including the diagnosis or the costs, and by patient data, such as their age or gender.
We can search by these fields and receive the hospitalization data that satisfies the query.

This engine complies with the need to access the information in a large dataset.
The data is visualized in a map through a heatmap and through clusters representations.
The colorful heatmap represents the intensity of the hospitalizations in each region.
On the other hand, the clusters represent the quantity of these hospitalizations, where the darker gray cluster contains more than 10k grouped data, the lighter gray contains less than 10k, and the small orange cluster (Figure \ref{fig:dengue_hospitalizations} shows it) contains the hospitalizations in the same census sector.

The second page is called \textbf{Health Facilities}.
This page contains more information on health facilities, such as its location at the map, its specialties, the number of beds, and so on.
Figure \ref{fig:health_facilities} shows an example containing the information of a hospital selected previously (\textit{Hospital Santo Antonio}).

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.9\textwidth]{health_facilities}
  \caption{Health Facilities page displaying Hospital Santo Antonio hospitalizations.}
  \label{fig:health_facilities}
\end{figure}

The \textbf{General Data} page is composed of a large variety of charts.
Each possible attribute of the database can be displayed in a chart on this page.
There are some examples in Figure \ref{fig:general_data}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.9\textwidth]{general_data}
  \caption{Part of the General Data page.}
  \label{fig:general_data}
\end{figure}

These features were designed together with the consultation of SMS-SP, so it intends to be a great tool for health public management purposes.

\section{Usage examples}
\label{sec:usage_examples}
To illustrate the platform usage, here we show how these features can be used to become an efficient tool for public health management.
Through this platform, the user can evaluate the health situation of specific areas, the hospitalizations distribution on the map over time, the area, the diagnosis, and over many other variables.
The user could also be aware of the distance traveled by the patient to be hospitalized.
This information can be retrieved via the Health Facilities page.

For example, Figure \ref{fig:health_facilities} shows on the map the location distribution of patients attended by \textit{Hospital Santo Antonio}.
Moreover, it shows the distance traveled by the patient by each hospitalization specialty on the right-side chart.
The last chart bar deep blue partition indicates that most of the surgical procedures corresponds to patients that traveled more than 10km to the medical procedure.

The use of the Advanced Search page to analyze the distribution of a specific diagnosis is another good example.
Figure \ref{fig:dengue_hospitalizations} shows the hospitalizations due to dengue, in 2015.
The user could retrieve this information over different periods and analyze the evolution of this scenario.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.9\textwidth]{dengue_hospitalizations}
  \caption{The platform displaying the hospitalizations due to dengue, in 2015.}
  \label{fig:dengue_hospitalizations}
\end{figure}

There are a large number of possibilities and combinations of information provided by this platform.
The Advanced Search page and General Data page offers many different variables to be combined and retrieved, such as patient diagnosis, age, race, gender, region, and so on.
As a result, this rich platform could produce good evidences to establish effective health policies.
